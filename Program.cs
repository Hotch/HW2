﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HW2 {
    class Program {
        static void Main (string[] args) {

            var crews = new List<Crew> (){
                new Crew () { Id = 1, EmployeeType = EmployeeType.PositivePilot, Name = "Hotch" },
                new Crew () { Id = 2, EmployeeType = EmployeeType.DeputyPilot, Name = "Jack" },
                new Crew () { Id = 3, EmployeeType = EmployeeType.CA, Name = "Nick" },
                new Crew () { Id = 4, EmployeeType = EmployeeType.CA, Name = "Mark" },
                new Crew () { Id = 5, EmployeeType = EmployeeType.CA, Name = "Joz" }
            };


            var flight = new Flight () {
                Id = "BR3030",
                Crew = crews
            };

            string output = string.Format ("{0} 機師是{1}和{2}，共有CA人數{3}人", flight.Id, flight.PositivePilot.Name, flight.DeputyPilot.Name, flight.Crew.Where (x => x.EmployeeType == EmployeeType.CA).ToList ().Count);

            Console.WriteLine (output);

        }
    }

    public class Flight {
        public string Id { get; set; }
        public List<Crew> Crew { get; set; }
        public Crew PositivePilot => Crew.FirstOrDefault (x => x.EmployeeType == EmployeeType.PositivePilot);
        public Crew DeputyPilot => Crew.FirstOrDefault (x => x.EmployeeType == EmployeeType.DeputyPilot);

    }
    public class Crew {

        public int Id { get; set; }
        public EmployeeType EmployeeType { get; set; }
        public string Name { get; set; }
    }

    public enum EmployeeType {
        PositivePilot,
        DeputyPilot,
        CA
    }
}